const { response } = require('express');
const { request } = require('http');
const pool = require('./../config/config');

module.exports = {
    getAll : async (request, response) => {
        pool.query('SELECT * FROM picture_frame', (error, result) => {
            if (error) throw error;
            response.send(result);
        });
    },

    getSpecific : async (request, response) => {
        const pk_id_picture_frame= parseInt(request.params.pk_id_picture_frame)
        let query = "";
        for(let i = 0; i < Object.keys(request.query).length; i++){
            query += `${Object.keys(request.query)[i]} = ${Object.values(request.query)[i]}`
            if(i+1 != Object.keys(request.query).length) query += " AND ";
            else query += ";"
        }
        pool.query('SELECT * FROM picture_frame WHERE pk_id_picture_frame = $1'+query, [pk_id_picture_frame], (error, result) => {
            if (error){
                throw error
              }

            response.send(result)
        });
    },

//insert ya quedo listo
    insert : async (request, response) => {
        const {type,url_picture_frame,description,color} = request.body
        
        pool.query('INSERT INTO picture_frame (type,url_picture_frame,description,color) VALUES ($1,$2,$3,$4)',
        [type,url_picture_frame,description,color], (error, result) =>{
            if (error){
                throw error
              }
            response.status(201).send('picture frame added');
        });
    },
//listo el update
    update : async (request, response) => {
        const pk_id_picture_frame = parseInt(request.params.pk_id_picture_frame)
        const {type,url_picture_frame,description,color} = request.body

        pool.query('UPDATE picture_frame SET type= $1, url_picture_frame = $2, description=$3, color = $4 WHERE pk_id_picture_frame = $5',
        [type,url_picture_frame,description,color, pk_id_picture_frame],
         (error, result) => {
            if (error) {
                throw error
              }

            response.send('row updated successfully.');
        });
    }, 
    //listo el delete
    delete : async (request, response) => {
        const pk_id_picture_frame = parseInt(request.params.pk_id_picture_frame)

        pool.query('DELETE FROM picture_frame WHERE pk_id_picture_frame = $1', [pk_id_picture_frame], (error, result) => {
            if (error) throw error;
            response.send('row deleted.');
        });
    }
    
}