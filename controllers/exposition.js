const { response } = require('express');
const { request } = require('http');
const pool = require('./../config/config');

module.exports = {
    getAll : async (request, response) => {
        pool.query('SELECT * FROM exposition', (error, result) => {
            if (error) throw error;
            response.send(result);
        });
    },

    getSpecific : async (request, response) => {
        const pk_id_exposition= parseInt(request.params.pk_id_exposition)
        let query = "";
        for(let i = 0; i < Object.keys(request.query).length; i++){
            query += `${Object.keys(request.query)[i]} = ${Object.values(request.query)[i]}`
            if(i+1 != Object.keys(request.query).length) query += " AND ";
            else query += ";"
        }
        pool.query('SELECT * FROM exposition WHERE pk_id_exposition = $1'+query, [pk_id_exposition], (error, result) => {
            if (error){
                throw error
              }

            response.send(result)
        });
    },

//insert ya quedo listo
    insert : async (request, response) => {
        const {name,image,private,duration_start,duration_end,music,description,fk_pieces_to_show, fk_curatorial_text} = request.body
        
        pool.query('INSERT INTO exposition (name,image,private,duration_start,duration_end,music,description,fk_pieces_to_show, fk_curatorial_text) VALUES ($1,$2,$3,$4,$5,$6,$7,$8,$9)',
         [name,image,private,duration_start,duration_end,music,description,fk_pieces_to_show, fk_curatorial_text], (error, result) =>{
            if (error){
                throw error
              }
            response.status(201).send('exposition added');
        });
    },
//listo el update
    update : async (request, response) => {
        const pk_id_exposition = parseInt(request.params.pk_id_exposition)
        const {name,image,private,duration_start,duration_end,music,description,fk_pieces_to_show, fk_curatorial_text} = request.body

        pool.query('UPDATE exposition SET name = $1, image = $2, private = $3, duration_start = $4,duration_end = $5, music = $6, description = $7, fk_pieces_to_show =$8, fk_curatorial_text = $9 WHERE pk_id_exposition = $10',
        [name,image,private,duration_start,duration_end,music,description,fk_pieces_to_show, fk_curatorial_text, pk_id_exposition],
         (error, result) => {
            if (error) {
                throw error
              }

            response.send('row updated successfully.');
        });
    }, 
    //listo el delete
    delete : async (request, response) => {
        const pk_id_exposition = parseInt(request.params.pk_id_exposition)

        pool.query('DELETE FROM exposition WHERE pk_id_exposition = $1', [pk_id_exposition], (error, result) => {
            if (error) throw error;
            response.send('row deleted.');
        });
    }
    
}