const { response } = require('express');
const { request } = require('http');
const pool = require('./../config/config');

module.exports = {
    getAll : async (request, response) => {
        pool.query('SELECT * FROM curatorial_text', (error, result) => {
            if (error) throw error;
            response.send(result);
        });
    },

    getSpecific : async (request, response) => {
        const pk_id_curatorial_text= parseInt(request.params.pk_id_curatorial_text)
        let query = "";
        for(let i = 0; i < Object.keys(request.query).length; i++){
            query += `${Object.keys(request.query)[i]} = ${Object.values(request.query)[i]}`
            if(i+1 != Object.keys(request.query).length) query += " AND ";
            else query += ";"
        }
        pool.query('SELECT * FROM curatorial_text WHERE pk_id_curatorial_text = $1'+query, [pk_id_curatorial_text], (error, result) => {
            if (error){
                throw error
              }

            response.send(result)
        });
    },

//insert ya quedo listo
    insert : async (request, response) => {
        const {name,description,fk_font_family,curatorial_image,font_size,pos_x,pos_y,pos_z	,rot_x,rot_y,rox_z,rot_w, scale_x,scale_y,scale_z} = request.body
        
        pool.query('INSERT INTO curatorial_text (name,description,fk_font_family,curatorial_image,font_size,pos_x,pos_y,pos_z	,rot_x,rot_y,rox_z,rot_w, scale_x,scale_y,scale_z) VALUES ($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12,$13,$14,$15)',
        [name,description,fk_font_family,curatorial_image,font_size,pos_x,pos_y,pos_z,rot_x,rot_y,rox_z,rot_w, scale_x,scale_y,scale_z],
         (error, result) =>{
            if (error){
                throw error
              }
            response.status(201).send('curatorial text added');
        });
    },
//listo el update
    update : async (request, response) => {
        const pk_id_curatorial_text = parseInt(request.params.pk_id_curatorial_text)
        const {name,description,fk_font_family,curatorial_image,font_size,pos_x,pos_y,pos_z	,rot_x,rot_y,rox_z,rot_w, scale_x,scale_y,scale_z} = request.body

        pool.query('UPDATE curatorial_text SET name = $1, description = $2, fk_font_family = $3, curatorial_image = $4, font_size = $5, pos_x = $6, pos_y = $7, pos_z = $8, rot_x = $9, rot_y = $10, rox_z = $11, rot_w = $12, scale_x = $13, scale_y = $14, scale_z = $15 WHERE pk_id_curatorial_text = $16',
        [name,description,fk_font_family,curatorial_image,font_size,pos_x,pos_y,pos_z	,rot_x,rot_y,rox_z,rot_w, scale_x,scale_y,scale_z, pk_id_curatorial_text],
         (error, result) => {
            if (error) {
                throw error
              }

            response.send('row updated successfully.');
        });
    }, 
    //listo el delete
    delete : async (request, response) => {
        const pk_id_curatorial_text = parseInt(request.params.pk_id_curatorial_text)

        pool.query('DELETE FROM curatorial_text WHERE pk_id_curatorial_text = $1', [pk_id_curatorial_text], (error, result) => {
            if (error) throw error;
            response.send('row deleted.');
        });
    }
    
}