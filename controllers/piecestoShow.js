const { response } = require('express');
const { request } = require('http');
const pool = require('./../config/config');

module.exports = {
    getAll : async (request, response) => {
        pool.query('SELECT * FROM pieces_to_show', (error, result) => {
            if (error) throw error;
            response.send(result);
        });
    },

    getSpecific : async (request, response) => {
        const pk_id_pieces_to_show= parseInt(request.params.pk_id_pieces_to_show)
        let query = "";
        for(let i = 0; i < Object.keys(request.query).length; i++){
            query += `${Object.keys(request.query)[i]} = ${Object.values(request.query)[i]}`
            if(i+1 != Object.keys(request.query).length) query += " AND ";
            else query += ";"
        }
        pool.query('SELECT * FROM pieces_to_show WHERE pk_id_pieces_to_show = $1'+query, [pk_id_pieces_to_show], (error, result) => {
            if (error){
                throw error
              }

            response.send(result)
        });
    },

//insert ya quedo listo
    insert : async (request, response) => {
        const {fk_aditional_info,fk_art_work} = request.body
        
        pool.query('INSERT INTO pieces_to_show (fk_aditional_info,fk_art_work) VALUES ($1,$2)',
        [fk_aditional_info,fk_art_work], (error, result) =>{
            if (error){
                throw error
              }
            response.status(201).send('pieces_to_show added');
        });
    },
//listo el update
    update : async (request, response) => {
        const pk_id_pieces_to_show = parseInt(request.params.pk_id_pieces_to_show)
        const {fk_aditional_info,fk_art_work} = request.body

        pool.query('UPDATE pieces_to_show SET fk_aditional_info= $1, fk_art_work = $2 WHERE pk_id_pieces_to_show = $3',
        [fk_aditional_info,fk_art_work, pk_id_pieces_to_show],
         (error, result) => {
            if (error) {
                throw error
              }

            response.send('row updated successfully.');
        });
    }, 
    //listo el delete
    delete : async (request, response) => {
        const pk_id_pieces_to_show = parseInt(request.params.pk_id_pieces_to_show)

        pool.query('DELETE FROM pieces_to_show WHERE pk_id_pieces_to_show = $1', [pk_id_pieces_to_show], (error, result) => {
            if (error) throw error;
            response.send('row deleted.');
        });
    }
    
}