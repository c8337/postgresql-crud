const { response } = require('express');
const { request } = require('http');
const pool = require('./../config/config');

module.exports = {
    getAll : async (request, response) => {
        pool.query('SELECT * FROM stats_artwork', (error, result) => {
            if (error) throw error;
            response.send(result);
        });
    },

    getSpecific : async (request, response) => {
        const pk_id_stats_artwork= parseInt(request.params.pk_id_stats_artwork)
        let query = "";
        for(let i = 0; i < Object.keys(request.query).length; i++){
            query += `${Object.keys(request.query)[i]} = ${Object.values(request.query)[i]}`
            if(i+1 != Object.keys(request.query).length) query += " AND ";
            else query += ";"
        }
        pool.query('SELECT * FROM stats_artwork WHERE pk_id_stats_artwork = $1'+query, [pk_id_stats_artwork], (error, result) => {
            if (error){
                throw error
              }

            response.send(result)
        });
    },

//insert ya quedo listo
    insert : async (request, response) => {
        const {likes, time_seen_artwork, total_views_art_card} = request.body
        
        pool.query('INSERT INTO stats_artwork (likes, time_seen_artwork, total_views_art_card) VALUES ($1,$2,$3)',[likes, time_seen_artwork, total_views_art_card], (error, result) =>{
            if (error){
                throw error
              }
            response.status(201).send('stat_artwork added');
        });
    },
//listo el update
    update : async (request, response) => {
        const pk_id_stats_artwork = parseInt(request.params.pk_id_stats_artwork)
        const {likes, time_seen_artwork, total_views_art_card} = request.body

        pool.query('UPDATE stats_artwork SET likes= $1, time_seen_artwork = $2,total_views_art_card=$3 WHERE pk_id_stats_artwork = $4',
        [likes, time_seen_artwork, total_views_art_card, pk_id_stats_artwork],
         (error, result) => {
            if (error) {
                throw error
              }

            response.send('row updated successfully.');
        });
    }, 
    //listo el delete
    delete : async (request, response) => {
        const pk_id_stats_artwork = parseInt(request.params.pk_id_stats_artwork)

        pool.query('DELETE FROM stats_artwork WHERE pk_id_stats_artwork = $1', [pk_id_stats_artwork], (error, result) => {
            if (error) throw error;
            response.send('row deleted.');
        });
    }
    
}