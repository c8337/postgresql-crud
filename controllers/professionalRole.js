const { response } = require('express');
const { request } = require('http');
const pool = require('./../config/config');

module.exports = {
    getAll : async (request, response) => {
        pool.query('SELECT * FROM professional_role', (error, result) => {
            if (error) throw error;
            response.send(result);
        });
    },

    getSpecific : async (request, response) => {
        const pk_id_professional_role= parseInt(request.params.pk_id_professional_role)
        let query = "";
        for(let i = 0; i < Object.keys(request.query).length; i++){
            query += `${Object.keys(request.query)[i]} = ${Object.values(request.query)[i]}`
            if(i+1 != Object.keys(request.query).length) query += " AND ";
            else query += ";"
        }
        pool.query('SELECT * FROM professional_role WHERE pk_id_professional_role = $1'+query, [pk_id_professional_role], (error, result) => {
            if (error){
                throw error
              }

            response.send(result)
        });
    },

//insert ya quedo listo
    insert : async (request, response) => {
        const {name} = request.body
        
        pool.query('INSERT INTO professional_role (name) VALUES ($1)',
        [name], (error, result) =>{
            if (error){
                throw error
              }
            response.status(201).send('professional_role added');
        });
    },
//listo el update
    update : async (request, response) => {
        const pk_id_professional_role = parseInt(request.params.pk_id_professional_role)
        const {name} = request.body

        pool.query('UPDATE professional_role SET name=$1  WHERE pk_id_professional_role = $2',
        [name,pk_id_professional_role],
         (error, result) => {
            if (error) {
                throw error
              }

            response.send('row updated successfully.');
        });
    }, 
    //listo el delete
    delete : async (request, response) => {
        const pk_id_professional_role = parseInt(request.params.pk_id_professional_role)

        pool.query('DELETE FROM professional_role WHERE pk_id_professional_role = $1', [pk_id_professional_role], (error, result) => {
            if (error) throw error;
            response.send('row deleted.');
        });
    }
    
}