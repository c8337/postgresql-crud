const { response } = require('express');
const { request } = require('http');
const pool = require('./../config/config');

module.exports = {
    getAll : async (request, response) => {
        pool.query('SELECT * FROM cart_item', (error, result) => {
            if (error) throw error;
            response.send(result);
        });
    },

    getSpecific : async (request, response) => {
        const pk_id_cart_item= parseInt(request.params.pk_id_cart_item)
        let query = "";
        for(let i = 0; i < Object.keys(request.query).length; i++){
            query += `${Object.keys(request.query)[i]} = ${Object.values(request.query)[i]}`
            if(i+1 != Object.keys(request.query).length) query += " AND ";
            else query += ";"
        }
        pool.query('SELECT * FROM cart_item WHERE pk_id_cart_item = $1'+query, [pk_id_cart_item], (error, result) => {
            if (error){
                throw error
              }

            response.send(result)
        });
    },

//insert ya quedo listo
    insert : async (request, response) => {
        const {fk_art_work,quantity} = request.body
        
        pool.query('INSERT INTO cart_item (fk_art_work,quantity) VALUES ($1,$2)',
         [fk_art_work,quantity], (error, result) =>{
            if (error){
                throw error
              }
            response.status(201).send('cart item added');
        });
    },
//listo el update
    update : async (request, response) => {
        const pk_id_cart_item = parseInt(request.params.pk_id_cart_item)
        const {fk_art_work,quantity} = request.body

        pool.query('UPDATE cart_item SET fk_art_work = $1, quantity = $2 WHERE pk_id_cart_item = $3',
        [fk_art_work,quantity, pk_id_cart_item],
         (error, result) => {
            if (error) {
                throw error
              }

            response.send('row updated successfully.');
        });
    }, 
    //listo el delete
    delete : async (request, response) => {
        const pk_id_cart_item = parseInt(request.params.pk_id_cart_item)

        pool.query('DELETE FROM cart_item WHERE pk_id_cart_item = $1', [pk_id_cart_item], (error, result) => {
            if (error) throw error;
            response.send('row deleted.');
        });
    }
    
}