const { response } = require('express');
const { request } = require('http');
const pool = require('./../config/config');

module.exports = {
    getAll : async (request, response) => {
        pool.query('SELECT * FROM measure_unit', (error, result) => {
            if (error) throw error;
            response.send(result);
        });
    },

    getSpecific : async (request, response) => {
        const pk_id_measure_unit= parseInt(request.params.pk_id_measure_unit)
        let query = "";
        for(let i = 0; i < Object.keys(request.query).length; i++){
            query += `${Object.keys(request.query)[i]} = ${Object.values(request.query)[i]}`
            if(i+1 != Object.keys(request.query).length) query += " AND ";
            else query += ";"
        }
        pool.query('SELECT * FROM measure_unit WHERE pk_id_measure_unit = $1'+query, [pk_id_measure_unit], (error, result) => {
            if (error){
                throw error
              }

            response.send(result)
        });
    },

//insert ya quedo listo
    insert : async (request, response) => {
        const {name,acronym} = request.body
        
        pool.query('INSERT INTO measure_unit (name,acronym) VALUES ($1,$2)',[name,acronym], (error, result) =>{
            if (error){
                throw error
              }
            response.status(201).send('measure_unit added');
        });
    },
//listo el update
    update : async (request, response) => {
        const pk_id_measure_unit = parseInt(request.params.pk_id_measure_unit)
        const {name,acronym} = request.body

        pool.query('UPDATE measure_unit SET name=$1, acronym = $2 WHERE pk_id_measure_unit = $3',
        [name,acronym, pk_id_measure_unit],
         (error, result) => {
            if (error) {
                throw error
              }

            response.send('row updated successfully.');
        });
    }, 
    //listo el delete
    delete : async (request, response) => {
        const pk_id_measure_unit = parseInt(request.params.pk_id_measure_unit)

        pool.query('DELETE FROM measure_unit WHERE pk_id_measure_unit = $1', [pk_id_measure_unit], (error, result) => {
            if (error) throw error;
            response.send('row deleted.');
        });
    }
    
}