const { response } = require('express');
const { request } = require('http');
const pool = require('./../config/config');

module.exports = {
    getAll : async (request, response) => {
        pool.query('SELECT * FROM auction', (error, result) => {
            if (error) throw error;
            response.send(result);
        });
    },

    getSpecific : async (request, response) => {
        const pk_id_auction= parseInt(request.params.pk_id_auction)
        let query = "";
        for(let i = 0; i < Object.keys(request.query).length; i++){
            query += `${Object.keys(request.query)[i]} = ${Object.values(request.query)[i]}`
            if(i+1 != Object.keys(request.query).length) query += " AND ";
            else query += ";"
        }
        pool.query('SELECT * FROM auction WHERE pk_id_auction = $1'+query, [pk_id_auction], (error, result) => {
            if (error){
                throw error
              }

            response.send(result)
        });
    },

//insert ya quedo listo
    insert : async (request, response) => {
        const {fk_art_work,higher_bet,bid_start_time,bid_end_time,base_price,increment_in_price_per_bid} = request.body
        
        pool.query('INSERT INTO auction (fk_art_work,higher_bet,bid_start_time,bid_end_time,base_price,increment_in_price_per_bid) VALUES ($1,$2,$3,$4,$5,$6)',
         [fk_art_work,higher_bet,bid_start_time,bid_end_time,base_price,increment_in_price_per_bid], (error, result) =>{
            if (error){
                throw error
              }
            response.status(201).send('auction added');
        });
    },
//listo el update
    update : async (request, response) => {
        const pk_id_auction = parseInt(request.params.pk_id_auction)
        const {fk_art_work,higher_bet,bid_start_time,bid_end_time,base_price,increment_in_price_per_bid} = request.body

        pool.query('UPDATE auction SET fk_art_work = $1, higher_bet = $2, bid_start_time = $3, bid_end_time = $4, base_price =$5, increment_in_price_per_bid = $6 WHERE pk_id_auction = $7',
        [fk_art_work,higher_bet,bid_start_time,bid_end_time,base_price,increment_in_price_per_bid, pk_id_auction],
         (error, result) => {
            if (error) {
                throw error
              }

            response.send('row updated successfully.');
        });
    }, 
    //listo el delete
    delete : async (request, response) => {
        const pk_id_auction = parseInt(request.params.pk_id_auction)

        pool.query('DELETE FROM auction WHERE pk_id_auction = $1', [pk_id_auction], (error, result) => {
            if (error) throw error;
            response.send('row deleted.');
        });
    }
    
}