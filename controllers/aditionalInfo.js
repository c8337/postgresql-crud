const { response } = require('express');
const { request } = require('http');
const pool = require('./../config/config');

module.exports = {
    getAll : async (request, response) => {
        pool.query('SELECT * FROM aditional_info', (error, result) => {
            if (error) throw error;
            response.send(result);
        });
    },

    getSpecific : async (request, response) => {
        const pk_id_aditional_info= parseInt(request.params.pk_id_aditional_info)
        let query = "";
        for(let i = 0; i < Object.keys(request.query).length; i++){
            query += `${Object.keys(request.query)[i]} = ${Object.values(request.query)[i]}`
            if(i+1 != Object.keys(request.query).length) query += " AND ";
            else query += ";"
        }
        pool.query('SELECT * FROM aditional_info WHERE pk_id_aditional_info = $1'+query, [pk_id_aditional_info], (error, result) => {
            if (error){
                throw error
              }

            response.send(result)
        });
    },

//insert ya quedo listo
    insert : async (request, response) => {
        const {pos_x,pos_y,pos_z,rot_x,rot_y,rot_w,scale_x,scale_y,scale_z,price,fk_currency,fk_picture_frame,fk_pedestal,fk_stats_artwork,has_explicit_content,country} = request.body
        
        pool.query('INSERT INTO aditional_info (pos_x,pos_y,pos_z,rot_x,rot_y,rot_w,scale_x,scale_y,scale_z,price,fk_currency,fk_picture_frame,fk_pedestal,fk_stats_artwork,has_explicit_content,country) VALUES ($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12,$13,$14,$15,$16)',
         [pos_x,pos_y,pos_z,rot_x,rot_y,rot_w,scale_x,scale_y,scale_z,price,fk_currency,fk_picture_frame,fk_pedestal,fk_stats_artwork,has_explicit_content,country], (error, result) =>{
            if (error){
                throw error
              }
            response.status(201).send('auction added');
        });
    },
//listo el update
    update : async (request, response) => {
        const pk_id_aditional_info = parseInt(request.params.pk_id_aditional_info)
        const {pos_x,pos_y,pos_z,rot_x,rot_y,rot_w,scale_x,scale_y,scale_z,price,fk_currency,fk_picture_frame,fk_pedestal,fk_stats_artwork,has_explicit_content,country} = request.body

        pool.query('UPDATE aditional_info SET pos_x = $1, pos_y = $2, pos_z = $3, rot_x = $4, rot_y = $5, rot_w = $6, scale_x = $7, scale_y = $8, scale_z = $9, price = $10, fk_currency = $11, fk_picture_frame = $12, fk_pedestal = $13, fk_stats_artwork = $14, has_explicit_content = $15, country = $16 WHERE pk_id_aditional_info = $17',
        [pos_x,pos_y,pos_z,rot_x,rot_y,rot_w,scale_x,scale_y,scale_z,price,fk_currency,fk_picture_frame,fk_pedestal,fk_stats_artwork,has_explicit_content,country, pk_id_aditional_info],
         (error, result) => {
            if (error) {
                throw error
              }

            response.send('row updated successfully.');
        });
    }, 
    //listo el delete
    delete : async (request, response) => {
        const pk_id_aditional_info = parseInt(request.params.pk_id_aditional_info)

        pool.query('DELETE FROM aditional_info WHERE pk_id_aditional_info = $1', [pk_id_aditional_info], (error, result) => {
            if (error) throw error;
            response.send('row deleted.');
        });
    }
    
}