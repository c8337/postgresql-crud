const { response } = require('express');
const { request } = require('http');
const pool = require('./../config/config');

module.exports = {
    getAll : async (request, response) => {
        pool.query('SELECT * FROM billing_cycle', (error, result) => {
            if (error) throw error;
            response.send(result);
        });
    },

    getSpecific : async (request, response) => {
        const pk_id_billing_cycle= parseInt(request.params.pk_id_billing_cycle)
        let query = "";
        for(let i = 0; i < Object.keys(request.query).length; i++){
            query += `${Object.keys(request.query)[i]} = ${Object.values(request.query)[i]}`
            if(i+1 != Object.keys(request.query).length) query += " AND ";
            else query += ";"
        }
        pool.query('SELECT * FROM billing_cycle WHERE pk_id_billing_cycle = $1'+query, [pk_id_billing_cycle], (error, result) => {
            if (error){
                throw error
              }

            response.send(result)
        });
    },

//insert ya quedo listo
    insert : async (request, response) => {
        const {time_to_bill} = request.body
        
        pool.query('INSERT INTO billing_cycle (time_to_bill) VALUES ($1)',
         [time_to_bill], (error, result) =>{
            if (error){
                throw error
              }
            response.status(201).send('billing cycle added');
        });
    },
//listo el update
    update : async (request, response) => {
        const pk_id_billing_cycle = parseInt(request.params.pk_id_billing_cycle)
        const {time_to_bill} = request.body

        pool.query('UPDATE billing_cycle SET time_to_bill = $1 WHERE pk_id_billing_cycle = $2',
        [time_to_bill, pk_id_billing_cycle],
         (error, result) => {
            if (error) {
                throw error
              }

            response.send('row updated successfully.');
        });
    }, 
    //listo el delete
    delete : async (request, response) => {
        const pk_id_billing_cycle = parseInt(request.params.pk_id_billing_cycle)

        pool.query('DELETE FROM billing_cycle WHERE pk_id_billing_cycle = $1', [pk_id_billing_cycle], (error, result) => {
            if (error) throw error;
            response.send('row deleted.');
        });
    }
    
}