const { response } = require('express');
const { request } = require('http');
const pool = require('./../config/config');

module.exports = {
    getAll : async (request, response) => {
        pool.query('SELECT * FROM billing_data', (error, result) => {
            if (error) throw error;
            response.send(result);
        });
    },

    getSpecific : async (request, response) => {
        const pk_id_billing_data= parseInt(request.params.pk_id_billing_data)
        let query = "";
        for(let i = 0; i < Object.keys(request.query).length; i++){
            query += `${Object.keys(request.query)[i]} = ${Object.values(request.query)[i]}`
            if(i+1 != Object.keys(request.query).length) query += " AND ";
            else query += ";"
        }
        pool.query('SELECT * FROM billing_data WHERE pk_id_billing_data = $1'+query, [pk_id_billing_data], (error, result) => {
            if (error){
                throw error
              }

            response.send(result)
        });
    },

//insert ya quedo listo
    insert : async (request, response) => {
        const {payment_day,due_date,fk_billing_cycle,id_transaction,pk_payment_method} = request.body
        
        pool.query('INSERT INTO billing_data (payment_day,due_date,fk_billing_cycle,id_transaction,pk_payment_method) VALUES ($1,$2,$3,$4,$5)',
         [payment_day,due_date,fk_billing_cycle,id_transaction,pk_payment_method], (error, result) =>{
            if (error){
                throw error
              }
            response.status(201).send('billing data added');
        });
    },
//listo el update
    update : async (request, response) => {
        const pk_id_billing_data = parseInt(request.params.pk_id_billing_data)
        const {payment_day,due_date,fk_billing_cycle,id_transaction,pk_payment_method} = request.body

        pool.query('UPDATE billing_data SET payment_day = $1, due_date = $2, fk_billing_cycle = $3, id_transaction = $4,pk_payment_method = $5 WHERE pk_id_billing_data = $6',
        [payment_day,due_date,fk_billing_cycle,id_transaction,pk_payment_method, pk_id_billing_data],
         (error, result) => {
            if (error) {
                throw error
              }

            response.send('row updated successfully.');
        });
    }, 
    //listo el delete
    delete : async (request, response) => {
        const pk_id_billing_data = parseInt(request.params.pk_id_billing_data)

        pool.query('DELETE FROM billing_data WHERE pk_id_billing_data = $1', [pk_id_billing_data], (error, result) => {
            if (error) throw error;
            response.send('row deleted.');
        });
    }
    
}