const { response } = require('express');
const { request } = require('http');
const pool = require('./../config/config');

module.exports = {
    getAll : async (request, response) => {
        pool.query('SELECT * FROM history_user', (error, result) => {
            if (error) throw error;
            response.send(result);
        });
    },

    getSpecific : async (request, response) => {
        const pk_id_history_user= parseInt(request.params.pk_id_history_user)
        let query = "";
        for(let i = 0; i < Object.keys(request.query).length; i++){
            query += `${Object.keys(request.query)[i]} = ${Object.values(request.query)[i]}`
            if(i+1 != Object.keys(request.query).length) query += " AND ";
            else query += ";"
        }
        pool.query('SELECT * FROM history_user WHERE pk_id_history_user = $1'+query, [pk_id_history_user], (error, result) => {
            if (error){
                throw error
              }

            response.send(result)
        });
    },

//insert ya quedo listo
    insert : async (request, response) => {
        const {total_time_in_platform,purchase_history,likes_history,artworks_purchase_history} = request.body
        
        pool.query('INSERT INTO history_user (total_time_in_platform,purchase_history,likes_history,artworks_purchase_history) VALUES ($1,$2,$3,$4)',
        [total_time_in_platform,purchase_history,likes_history,artworks_purchase_history], (error, result) =>{
            if (error){
                throw error
              }
            response.status(201).send('history_user added');
        });
    },
//listo el update
    update : async (request, response) => {
        const pk_id_history_user = parseInt(request.params.pk_id_history_user)
        const {total_time_in_platform,purchase_history,likes_history,artworks_purchase_history} = request.body

        pool.query('UPDATE history_user SET total_time_in_platform = $1, purchase_history = $2,likes_history =$3,artworks_purchase_history=$4 WHERE pk_id_history_user = $5',
        [total_time_in_platform,purchase_history,likes_history,artworks_purchase_history, pk_id_history_user],
         (error, result) => {
            if (error) {
                throw error
              }

            response.send('row updated successfully.');
        });
    }, 
    //listo el delete
    delete : async (request, response) => {
        const pk_id_history_user = parseInt(request.params.pk_id_history_user)

        pool.query('DELETE FROM history_user WHERE pk_id_history_user = $1', [pk_id_history_user], (error, result) => {
            if (error) throw error;
            response.send('row deleted.');
        });
    }
    
}