const { response } = require('express');
const { request } = require('http');
const pool = require('./../config/config');

module.exports = {
    getAll : async (request, response) => {
        pool.query('SELECT * FROM currency', (error, result) => {
            if (error) throw error;
            response.send(result);
        });
    },

    getSpecific : async (request, response) => {
        const pk_id_currency= parseInt(request.params.pk_id_currency)
        let query = "";
        for(let i = 0; i < Object.keys(request.query).length; i++){
            query += `${Object.keys(request.query)[i]} = ${Object.values(request.query)[i]}`
            if(i+1 != Object.keys(request.query).length) query += " AND ";
            else query += ";"
        }
        pool.query('SELECT * FROM currency WHERE pk_id_currency = $1'+query, [pk_id_currency], (error, result) => {
            if (error){
                throw error
              }

            response.send(result)
        });
    },

//insert ya quedo listo
    insert : async (request, response) => {
        const {name} = request.body
        
        pool.query('INSERT INTO currency (name) VALUES ($1)',
         [name], (error, result) =>{
            if (error){
                throw error
              }
            response.status(201).send('currency added');
        });
    },
//listo el update
    update : async (request, response) => {
        const pk_id_currency = parseInt(request.params.pk_id_currency)
        const {name} = request.body

        pool.query('UPDATE currency SET name = $1 WHERE pk_id_currency = $2',
        [name, pk_id_currency],
         (error, result) => {
            if (error) {
                throw error
              }

            response.send('row updated successfully.');
        });
    }, 
    //listo el delete
    delete : async (request, response) => {
        const pk_id_currency = parseInt(request.params.pk_id_currency)

        pool.query('DELETE FROM currency WHERE pk_id_currency = $1', [pk_id_currency], (error, result) => {
            if (error) throw error;
            response.send('row deleted.');
        });
    }
    
}