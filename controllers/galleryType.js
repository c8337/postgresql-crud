const { response } = require('express');
const { request } = require('http');
const pool = require('./../config/config');

module.exports = {
    getAll : async (request, response) => {
        pool.query('SELECT * FROM gallery_type', (error, result) => {
            if (error) throw error;
            response.send(result);
        });
    },

    getSpecific : async (request, response) => {
        const pk_id_gallery_type= parseInt(request.params.pk_id_gallery_type)
        let query = "";
        for(let i = 0; i < Object.keys(request.query).length; i++){
            query += `${Object.keys(request.query)[i]} = ${Object.values(request.query)[i]}`
            if(i+1 != Object.keys(request.query).length) query += " AND ";
            else query += ";"
        }
        pool.query('SELECT * FROM gallery_type WHERE pk_id_gallery_type = $1'+query, [pk_id_gallery_type], (error, result) => {
            if (error){
                throw error
              }

            response.send(result)
        });
    },

//insert ya quedo listo
    insert : async (request, response) => {
        const {name,total_pieces,dimentions_m2,description} = request.body
        
        pool.query('INSERT INTO gallery_type (name,total_pieces,dimentions_m2,description) VALUES ($1,$2,$3,$4)',
        [name,total_pieces,dimentions_m2,description], (error, result) =>{
            if (error){
                throw error
              }
            response.status(201).send('gallery_type added');
        });
    },
//listo el update
    update : async (request, response) => {
        const pk_id_gallery_type = parseInt(request.params.pk_id_gallery_type)
        const {name,total_pieces,dimentions_m2,description} = request.body

        pool.query('UPDATE gallery_type SET name=$1, total_pieces = $2, dimentions_m2 = $3,description=$4 WHERE pk_id_gallery_type = $5',
        [name,total_pieces,dimentions_m2,description, pk_id_gallery_type],
         (error, result) => {
            if (error) {
                throw error
              }

            response.send('row updated successfully.');
        });
    }, 
    //listo el delete
    delete : async (request, response) => {
        const pk_id_gallery_type = parseInt(request.params.pk_id_gallery_type)

        pool.query('DELETE FROM gallery_type WHERE pk_id_gallery_type = $1', [pk_id_gallery_type], (error, result) => {
            if (error) throw error;
            response.send('row deleted.');
        });
    }
    
}