const { response } = require('express');
const { request } = require('http');
const { parse } = require('path');
const pool = require('./../config/config');

module.exports = {
    getAll : async (request, response) => {
        pool.query('SELECT * FROM pedestal', (error, result) => {
            if (error) throw error;
            response.send(result);
        });
    },

    getSpecific : async (request, response) => {
        const pk_id_pedestal= parseInt(request.params.pk_id_pedestal)
        let query = "";
        for(let i = 0; i < Object.keys(request.query).length; i++){
            query += `${Object.keys(request.query)[i]} = ${Object.values(request.query)[i]}`
            if(i+1 != Object.keys(request.query).length) query += " AND ";
            else query += ";"
        }
        pool.query('SELECT * FROM pedestal WHERE pk_id_pedestal = $1'+query, [pk_id_pedestal], (error, result) => {
            if (error){
                throw error
              }

            response.send(result)
        });
    },

//insert ya quedo listo
    insert : async (request, response) => {
        const {type, url_pedestal,description,color} = request.body
        
        pool.query('INSERT INTO pedestal (type, url_pedestal,description,color) VALUES ($1,$2,$3,$4)',[type, url_pedestal,description,color], (error, result) =>{
            if (error){
                throw error
              }
            response.status(201).send('pedestal added');
        });
    },
//listo el update
    update : async (request, response) => {
        const pk_id_pedestal = parseInt(request.params.pk_id_pedestal)
        const {type, url_pedestal,description,color} = request.body

        pool.query('UPDATE pedestal SET type = $1, url_pedestal = $2, description = $3, color = $4 WHERE pk_id_pedestal = $5',
        [type, url_pedestal,description,color, pk_id_pedestal],
         (error, result) => {
            if (error) {
                throw error
              }

            response.send('row updated successfully.');
        });
    }, 
    //listo el delete
    delete : async (request, response) => {
        const pk_id_pedestal = parseInt(request.params.pk_id_pedestal)

        pool.query('DELETE FROM pedestal WHERE pk_id_pedestal = $1', [pk_id_pedestal], (error, result) => {
            if (error) throw error;
            response.send('row deleted.');
        });
    }
    
}