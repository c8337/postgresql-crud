const { response } = require('express');
const { request } = require('http');
const pool = require('./../config/config');

module.exports = {
    getAll : async (request, response) => {
        pool.query('SELECT * FROM bids', (error, result) => {
            if (error) throw error;
            response.send(result);
        });
    },

    getSpecific : async (request, response) => {
        const pk_id_bid= parseInt(request.params.pk_id_bid)
        let query = "";
        for(let i = 0; i < Object.keys(request.query).length; i++){
            query += `${Object.keys(request.query)[i]} = ${Object.values(request.query)[i]}`
            if(i+1 != Object.keys(request.query).length) query += " AND ";
            else query += ";"
        }
        pool.query('SELECT * FROM bids WHERE pk_id_bid = $1'+query, [pk_id_bid], (error, result) => {
            if (error){
                throw error
              }

            response.send(result)
        });
    },

//insert ya quedo listo
    insert : async (request, response) => {
        const {fk_user,fk_auction,bid_amount,bid_time} = request.body
        
        pool.query('INSERT INTO bids (fk_user,fk_auction,bid_amount,bid_time) VALUES ($1,$2,$3,$4)',
         [fk_user,fk_auction,bid_amount,bid_time], (error, result) =>{
            if (error){
                throw error
              }
            response.status(201).send('bid added');
        });
    },
//listo el update
    update : async (request, response) => {
        const pk_id_bid = parseInt(request.params.pk_id_bid)
        const {fk_user,fk_auction,bid_amount,bid_time} = request.body

        pool.query('UPDATE bids SET fk_user = $1, fk_auction = $2, bid_amount = $3, bid_time = $4 WHERE pk_id_bid = $5',
        [fk_user,fk_auction,bid_amount,bid_time, pk_id_bid],
         (error, result) => {
            if (error) {
                throw error
              }

            response.send('row updated successfully.');
        });
    }, 
    //listo el delete
    delete : async (request, response) => {
        const pk_id_bid = parseInt(request.params.pk_id_bid)

        pool.query('DELETE FROM bids WHERE pk_id_bid = $1', [pk_id_bid], (error, result) => {
            if (error) throw error;
            response.send('row deleted.');
        });
    }
    
}