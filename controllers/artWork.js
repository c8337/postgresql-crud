const { response } = require('express');
const { request } = require('http');
const pool = require('./../config/config');

module.exports = {
    getAll : async (request, response) => {
        pool.query('SELECT * FROM art_work', (error, result) => {
            if (error) throw error;
            response.send(result);
        });
    },

    getSpecific : async (request, response) => {
        const pk_id_art_work= parseInt(request.params.pk_id_art_work)
        let query = "";
        for(let i = 0; i < Object.keys(request.query).length; i++){
            query += `${Object.keys(request.query)[i]} = ${Object.values(request.query)[i]}`
            if(i+1 != Object.keys(request.query).length) query += " AND ";
            else query += ";"
        }
        pool.query('SELECT * FROM art_work WHERE pk_id_art_work = $1'+query, [pk_id_art_work], (error, result) => {
            if (error){
                throw error
              }

            response.send(result)
        });
    },

//insert ya quedo listo
    insert : async (request, response) => {
        const {work_title,name_artist_colective,url_artwork,year,description,fk_measure_unit,measure_hight,measure_width,measure_depth,technique,total_copies,fk_status,fk_aditional_info,fk_category_art_work,created_date} = request.body
        
        pool.query('INSERT INTO art_work (work_title,name_artist_colective,url_artwork,year,description,fk_measure_unit,measure_hight,measure_width,measure_depth,technique,total_copies,fk_status,fk_aditional_info,fk_category_art_work,created_date) VALUES ($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12,$13,$14,$15)',
         [work_title,name_artist_colective,url_artwork,year,description,fk_measure_unit,measure_hight,measure_width,measure_depth,technique,total_copies,fk_status,fk_aditional_info,fk_category_art_work,created_date], (error, result) =>{
            if (error){
                throw error
              }
            response.status(201).send('auction added');
        });
    },
//listo el update
    update : async (request, response) => {
        const pk_id_art_work = parseInt(request.params.pk_id_art_work)
        const {work_title,name_artist_colective,url_artwork,year,description,fk_measure_unit,measure_hight,measure_width,measure_depth,technique,total_copies,fk_status,fk_aditional_info,fk_category_art_work,created_date} = request.body

        pool.query('UPDATE art_work SET work_title = $1, name_artist_colective = $2, url_artwork = $3, year = $4, description = $5, fk_measure_unit = $6, measure_hight = $7, measure_width = $8, measure_depth = $9, technique = $10, total_copies = $11, fk_status = $12, fk_aditional_info = $13, fk_category_art_work = $14, created_date = $15 WHERE pk_id_art_work = $16',
        [work_title,name_artist_colective,url_artwork,year,description,fk_measure_unit,measure_hight,measure_width,measure_depth,technique,total_copies,fk_status,fk_aditional_info,fk_category_art_work,created_date, pk_id_art_work],
         (error, result) => {
            if (error) {
                throw error
              }

            response.send('row updated successfully.');
        });
    }, 
    //listo el delete
    delete : async (request, response) => {
        const pk_id_art_work = parseInt(request.params.pk_id_art_work)

        pool.query('DELETE FROM art_work WHERE pk_id_art_work = $1', [pk_id_art_work], (error, result) => {
            if (error) throw error;
            response.send('row deleted.');
        });
    }
    
}