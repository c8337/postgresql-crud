const { response } = require('express');
const { request } = require('http');
const pool = require('./../config/config');

module.exports = {
    getAll : async (request, response) => {
        pool.query('SELECT * FROM users', (error, result) => {
            if (error) throw error;
            response.send(result);
        });
    },

    getSpecific : async (request, response) => {
        const pk_id_users= parseInt(request.params.pk_id_users)
        let query = "";
        for(let i = 0; i < Object.keys(request.query).length; i++){
            query += `${Object.keys(request.query)[i]} = ${Object.values(request.query)[i]}`
            if(i+1 != Object.keys(request.query).length) query += " AND ";
            else query += ";"
        }
        pool.query('SELECT * FROM users WHERE pk_id_users = $1'+query, [pk_id_users], (error, result) => {
            if (error){
                throw error
              }

            response.send(result)
        });
    },

//insert ya quedo listo
    insert : async (request, response) => {
        const {username, name, email,password, country, city, image, fk_professional_role,
            description, link_portfolio, joining_date, fk_history_user, last_login} = request.body
        
        pool.query('INSERT INTO users (username, name, email, password, country, city, image, fk_professional_role, description, link_portfolio, joining_date, fk_history_user, last_login) VALUES ($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12,$13)',
        [username, name, email,password, country, city, image, fk_professional_role,
         description, link_portfolio, joining_date, fk_history_user, last_login],
         (error, result) =>{
            if (error){
                throw error
              }
            response.status(201).send('user added');
        });
    },
//listo el update
    update : async (request, response) => {
        const pk_id_users = parseInt(request.params.pk_id_users)
        const {username, name, email,password, country, city, image, fk_professional_role,
            description, link_portfolio, joining_date, fk_history_user, last_login} = request.body

        pool.query('UPDATE users SET username=$1, name=$2, email=$3,password=$4, country=$5, city=$6, image=$7, fk_professional_role=$8, description=$9, link_portfolio=$10, joining_date=$11, fk_history_user=$12, last_login=$13 WHERE pk_id_users = $14',
        [username, name, email,password, country, city, image, fk_professional_role,
            description, link_portfolio, joining_date, fk_history_user, last_login,pk_id_users],
         (error, result) => {
            if (error) {
                throw error
              }

            response.send('row updated successfully.');
        });
    }, 
    //listo el delete
    delete : async (request, response) => {
        const pk_id_users = parseInt(request.params.pk_id_users)

        pool.query('DELETE FROM users WHERE pk_id_users = $1', [pk_id_users], (error, result) => {
            if (error) throw error;
            response.send('row deleted.');
        });
    }
    
}


