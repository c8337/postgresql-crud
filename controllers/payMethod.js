const { response } = require('express');
const { request } = require('http');
const pool = require('./../config/config');

module.exports = {
    getAll : async (request, response) => {
        pool.query('SELECT * FROM payment_methods', (error, result) => {
            if (error) throw error;
            response.send(result);
        });
    },

    getSpecific : async (request, response) => {
        const pk_id_payment_method= parseInt(request.params.pk_id_payment_method)
        let query = "";
        for(let i = 0; i < Object.keys(request.query).length; i++){
            query += `${Object.keys(request.query)[i]} = ${Object.values(request.query)[i]}`
            if(i+1 != Object.keys(request.query).length) query += " AND ";
            else query += ";"
        }
        pool.query('SELECT * FROM payment_methods WHERE pk_id_payment_method = $1'+query, [pk_id_payment_method], (error, result) => {
            if (error){
                throw error
              }

            response.send(result)
        });
    },

//insert ya quedo listo
    insert : async (request, response) => {
        const {payment_description} = request.body
        
        pool.query('INSERT INTO payment_methods (payment_description) VALUES ($1)',[payment_description], (error, result) =>{
            if (error){
                throw error
              }
            response.status(201).send('pay_method added');
        });
    },
//listo el update
    update : async (request, response) => {
        const pk_id_payment_method = parseInt(request.params.pk_id_payment_method)
        const {payment_description} = request.body

        pool.query('UPDATE payment_methods SET payment_description = $1 WHERE pk_id_payment_method = $2',
        [payment_description, pk_id_payment_method],
         (error, result) => {
            if (error) {
                throw error
              }

            response.send('row updated successfully.');
        });
    }, 
    //listo el delete
    delete : async (request, response) => {
        const pk_id_payment_method = parseInt(request.params.pk_id_payment_method)

        pool.query('DELETE FROM payment_methods WHERE pk_id_payment_method = $1', [pk_id_payment_method], (error, result) => {
            if (error) throw error;
            response.send('row deleted.');
        });
    }
    
}