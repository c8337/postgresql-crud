const { response } = require('express');
const { request } = require('http');
const pool = require('./../config/config');

module.exports = {
    getAll : async (request, response) => {
        pool.query('SELECT * FROM delivery_addresses', (error, result) => {
            if (error) throw error;
            response.send(result);
        });
    },

    getSpecific : async (request, response) => {
        const pk_id_delivery= parseInt(request.params.pk_id_delivery)
        let query = "";
        for(let i = 0; i < Object.keys(request.query).length; i++){
            query += `${Object.keys(request.query)[i]} = ${Object.values(request.query)[i]}`
            if(i+1 != Object.keys(request.query).length) query += " AND ";
            else query += ";"
        }
        pool.query('SELECT * FROM delivery_addresses WHERE pk_id_delivery = $1'+query, [pk_id_delivery], (error, result) => {
            if (error){
                throw error
              }

            response.send(result)
        });
    },

//insert ya quedo listo
    insert : async (request, response) => {
        const {country,city,address_1,address_2,postcode,phone,email} = request.body
        
        pool.query('INSERT INTO delivery_addresses (country,city,address_1,address_2,postcode,phone,email) VALUES ($1,$2,$3,$4,$5,$6,$7)',
         [country,city,address_1,address_2,postcode,phone,email], (error, result) =>{
            if (error){
                throw error
              }
            response.status(201).send('delivery address added');
        });
    },
//listo el update
    update : async (request, response) => {
        const pk_id_delivery = parseInt(request.params.pk_id_delivery)
        const {country,city,address_1,address_2,postcode,phone,email} = request.body

        pool.query('UPDATE delivery_addresses SET country = $1, city = $2, address_1 =$3, address_2 = $4, postcode = $5,phone = $6, email = $7 WHERE pk_id_delivery = $8',
        [country,city,address_1,address_2,postcode,phone,email, pk_id_delivery],
         (error, result) => {
            if (error) {
                throw error
              }

            response.send('row updated successfully.');
        });
    }, 
    //listo el delete
    delete : async (request, response) => {
        const pk_id_delivery = parseInt(request.params.pk_id_delivery)

        pool.query('DELETE FROM delivery_addresses WHERE pk_id_delivery = $1', [pk_id_delivery], (error, result) => {
            if (error) throw error;
            response.send('row deleted.');
        });
    }
    
}