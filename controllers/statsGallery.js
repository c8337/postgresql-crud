const { response } = require('express');
const { request } = require('http');
const pool = require('./../config/config');

module.exports = {
    getAll : async (request, response) => {
        pool.query('SELECT * FROM stats_gallery', (error, result) => {
            if (error) throw error;
            response.send(result);
        });
    },

    getSpecific : async (request, response) => {
        const pk_id_stats_gallery= parseInt(request.params.pk_id_stats_gallery)
        let query = "";
        for(let i = 0; i < Object.keys(request.query).length; i++){
            query += `${Object.keys(request.query)[i]} = ${Object.values(request.query)[i]}`
            if(i+1 != Object.keys(request.query).length) query += " AND ";
            else query += ";"
        }
        pool.query('SELECT * FROM stats_gallery WHERE pk_id_stats_gallery = $1'+query, [pk_id_stats_gallery], (error, result) => {
            if (error){
                throw error
              }

            response.send(result)
        });
    },

//insert ya quedo listo
    insert : async (request, response) => {
        const {total_visits,average_time,country_visit} = request.body
        
        pool.query('INSERT INTO stats_gallery (total_visits,average_time,country_visit) VALUES ($1,$2,$3)',[total_visits,average_time,country_visit], (error, result) =>{
            if (error){
                throw error
              }
            response.status(201).send('stat_gallery added');
        });
    },
//listo el update
    update : async (request, response) => {
        const pk_id_stats_gallery = parseInt(request.params.pk_id_stats_gallery)
        const {total_visits,average_time,country_visit} = request.body

        pool.query('UPDATE stats_gallery SET total_visits= $1, average_time = $2,country_visit=$3 WHERE pk_id_stats_gallery = $4',
        [total_visits,average_time,country_visit, pk_id_stats_gallery],
         (error, result) => {
            if (error) {
                throw error
              }

            response.send('row updated successfully.');
        });
    }, 
    //listo el delete
    delete : async (request, response) => {
        const pk_id_stats_gallery = parseInt(request.params.pk_id_stats_gallery)

        pool.query('DELETE FROM stats_gallery WHERE pk_id_stats_gallery = $1', [pk_id_stats_gallery], (error, result) => {
            if (error) throw error;
            response.send('row deleted.');
        });
    }
    
}