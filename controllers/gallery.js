const { response } = require('express');
const { request } = require('http');
const pool = require('./../config/config');

module.exports = {
    getAll : async (request, response) => {
        pool.query('SELECT * FROM gallery', (error, result) => {
            if (error) throw error;
            response.send(result);
        });
    },

    getSpecific : async (request, response) => {
        const pk_id_gallery= parseInt(request.params.pk_id_gallery)
        let query = "";
        for(let i = 0; i < Object.keys(request.query).length; i++){
            query += `${Object.keys(request.query)[i]} = ${Object.values(request.query)[i]}`
            if(i+1 != Object.keys(request.query).length) query += " AND ";
            else query += ";"
        }
        pool.query('SELECT * FROM gallery WHERE pk_id_gallery = $1'+query, [pk_id_gallery], (error, result) => {
            if (error){
                throw error
              }

            response.send(result)
        });
    },

//insert ya quedo listo
    insert : async (request, response) => {
        const {name,price,image,description,fk_user,fk_billing_data,fk_gallery_type,fk_stats_gallery,fk_exposition} = request.body
        
        pool.query('INSERT INTO gallery (name,price,image,description,fk_user,fk_billing_data,fk_gallery_type,fk_stats_gallery,fk_exposition) VALUES ($1,$2,$3,$4,$5,$6,$7,$8,$9)',
        [name,price,image,description,fk_user,fk_billing_data,fk_gallery_type,fk_stats_gallery,fk_exposition], (error, result) =>{
            if (error){
                throw error
              }
            response.status(201).send('gallery added');
        });
    },
//listo el update
    update : async (request, response) => {
        const pk_id_gallery = parseInt(request.params.pk_id_gallery)
        const {name,price,image,description,fk_user,fk_billing_data,fk_gallery_type,fk_stats_gallery,fk_exposition} = request.body

        pool.query('UPDATE gallery SET name = $1, price = $2, image =$3 , description = $4, fk_user = $5, fk_billing_data = $6, fk_gallery_type = $7, fk_stats_gallery = $8, fk_exposition = $9 WHERE pk_id_gallery = $10',
        [name,price,image,description,fk_user,fk_billing_data,fk_gallery_type,fk_stats_gallery,fk_exposition, pk_id_gallery],
         (error, result) => {
            if (error) {
                throw error
              }

            response.send('row updated successfully.');
        });
    }, 
    //listo el delete
    delete : async (request, response) => {
        const pk_id_gallery = parseInt(request.params.pk_id_gallery)

        pool.query('DELETE FROM gallery WHERE pk_id_gallery = $1', [pk_id_gallery], (error, result) => {
            if (error) throw error;
            response.send('row deleted.');
        });
    }
    
}