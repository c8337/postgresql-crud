const { response } = require('express');
const { request } = require('http');
const pool = require('./../config/config');

module.exports = {
    getAll : async (request, response) => {
        pool.query('SELECT * FROM shopping_cart', (error, result) => {
            if (error) throw error;
            response.send(result);
        });
    },

    getSpecific : async (request, response) => {
        const pk_id_shopping_cart= parseInt(request.params.pk_id_shopping_cart)
        let query = "";
        for(let i = 0; i < Object.keys(request.query).length; i++){
            query += `${Object.keys(request.query)[i]} = ${Object.values(request.query)[i]}`
            if(i+1 != Object.keys(request.query).length) query += " AND ";
            else query += ";"
        }
        pool.query('SELECT * FROM shopping_cart WHERE pk_id_shopping_cart = $1'+query, [pk_id_shopping_cart], (error, result) => {
            if (error){
                throw error
              }

            response.send(result)
        });
    },

//insert ya quedo listo
    insert : async (request, response) => {
        const {fk_cart_item, fk_users, fk_payment_methods, fk_delivery_addresses, total} = request.body
        
        pool.query('INSERT INTO shopping_cart (fk_cart_item, fk_users, fk_payment_methods, fk_delivery_addresses, total) VALUES ($1,$2,$3,$4,$5)',
        [fk_cart_item, fk_users, fk_payment_methods, fk_delivery_addresses, total], (error, result) =>{
            if (error){
                throw error
              }
            response.status(201).send('shopping_cart added');
        });
    },
//listo el update
    update : async (request, response) => {
        const pk_id_shopping_cart = parseInt(request.params.pk_id_shopping_cart)
        const {fk_cart_item, fk_users, fk_payment_methods, fk_delivery_addresses, total} = request.body

        pool.query('UPDATE shopping_cart SET fk_cart_item= $1, fk_users = $2,fk_payment_methods=$3, fk_delivery_addresses=$4, total=$5  WHERE pk_id_shopping_cart = $6',
        [fk_cart_item, fk_users, fk_payment_methods, fk_delivery_addresses, total,pk_id_shopping_cart],
         (error, result) => {
            if (error) {
                throw error
              }

            response.send('row updated successfully.');
        });
    }, 
    //listo el delete
    delete : async (request, response) => {
        const pk_id_shopping_cart = parseInt(request.params.pk_id_shopping_cart)

        pool.query('DELETE FROM shopping_cart WHERE pk_id_shopping_cart = $1', [pk_id_shopping_cart], (error, result) => {
            if (error) throw error;
            response.send('row deleted.');
        });
    }
    
}