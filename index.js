const express = require("express");
const cors = require("cors");
const bodyParser = require("body-parser");
const getMethod = require("./routes/getQuery");
const postMethod = require("./routes/postQuery");
const deleteMethod = require("./routes/deleteQuery");
const updateMethod = require("./routes/putQuery");
const app = express();

app.use(cors({ origin: true, credentials: true }));
app.use(express.json());
app.use(express.urlencoded({extended: true}));
app.use("/get", getMethod); //poner el get/ y luego el que siga /user por ejemplo
app.use("/post", postMethod);
app.use("/delete", deleteMethod);
app.use("/put", updateMethod);

//pruebas
app.get("/", function(req, res){
    res.send("Escribe la ruta, despues del 7007/");
});



app.listen(7007, () => {
    console.log("Inicializando el servicio en el puerto 7007");
});