const { prototype } = require('pg/lib/connection');
const {Pool} = require('pg');
//const mysql = require('mysql2');
// Set database connection credentials
const config = {
    //user: 'postgres',
    user: 'ubuntu',
    //host: 'containdb.cqmfvyrw2ank.us-west-2.rds.amazonaws.com',
    host: 'localhost',
    //database: 'containDB',
    database: 'containdb',
    password: 'user1234',
    port: 5432,
};

// Create a MySQL pool
const pool = new Pool(config);
// const pool = pg.createPool(config);

// Export the pool
module.exports = pool;
