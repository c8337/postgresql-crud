const express = require("express");
const router = express.Router();
const payMethod = require("./../controllers/payMethod");
const piecestoShow = require("./../controllers/piecestoShow");
const status = require("./../controllers/status");
const users = require("./../controllers/users");
const professionalRole = require("./../controllers/professionalRole");
const historyUser = require("./../controllers/historyUser");
const gallery = require("./../controllers/gallery");
const galleryType = require("./../controllers/galleryType");
const statsGallery = require("./../controllers/statsGallery");
const exposition = require("./../controllers/exposition");
const curatorialText = require("./../controllers/curatorialText");
const fontFamily = require("./../controllers/fontFamily");
const artWork = require("./../controllers/artWork");
const measureUnit = require("./../controllers/measureUnit");
const categoryArtwork = require("./../controllers/categoryArtwork");
const aditionalInfo = require("./../controllers/aditionalInfo");
const pictureFrame = require("./../controllers/pictureFrame");
const pedestal = require("./../controllers/pedestal");
const currency = require("./../controllers/currency");
const statsArtwork = require("./../controllers/statsArtwork");
const shoppingCart = require("./../controllers/shoppingCart");
const deliveryAddress = require("./../controllers/deliveryAddress");
const cartItem = require("./../controllers/cartItem");
const auction = require("./../controllers/auction");
const bids = require("./../controllers/bids");
const billingCycle = require("./../controllers/billingCycle");
const billingData = require("./../controllers/billingData");

router.put("/payMethod/:pk_id_payment_method", payMethod.update);//listo
router.put("/piecestoShow/:pk_id_pieces_to_show", piecestoShow.update);//listo
router.put("/users/:pk_id_users", users.update);//listo
router.put("/status/:pk_id_status", status.update);//listo
router.put("/professionalRole/:pk_id_professional_role", professionalRole.update);//listo
router.put("/historyUser/:pk_id_history_user", historyUser.update);//listo
router.put("/gallery/:pk_id_gallery", gallery.update);//listo
router.put("/galleryType/:pk_id_gallery_type", galleryType.update);//listo
router.put("/statsGallery/:pk_id_stats_gallery", statsGallery.update);//listo
router.put("/exposition/:pk_id_exposition", exposition.update);//listo
router.put("/curatorialText/:pk_id_curatorial_text", curatorialText.update);//listo
router.put("/fontFamily/:pk_id_font_family", fontFamily.update);//listo
router.put("/artWork/:pk_id_art_work", artWork.update);//listo
router.put("/measureUnit/:pk_id_measure_unit", measureUnit.update);//listo
router.put("/categoryArtwork/:pk_id_category_art_work", categoryArtwork.update);//listo
router.put("/aditionalInfo/:pk_id_aditional_info", aditionalInfo.update);//listo
router.put("/pictureFrame/:pk_id_picture_frame", pictureFrame.update);//listo
router.put("/pedestal/:pk_id_pedestal", pedestal.update);//listo
router.put("/currency/:pk_id_currency", currency.update);//listo
router.put("/statsArtwork/:pk_id_stats_artwork", statsArtwork.update);//listo
router.put("/shoppingCart/:pk_id_shopping_cart", shoppingCart.update);//listo
router.put("/deliveryAddress/:pk_id_delivery", deliveryAddress.update);//listo
router.put("/cartItem/:pk_id_cart_item", cartItem.update);//listo
router.put("/auction/:pk_id_auction", auction.update);//listo
router.put("/bids/:pk_id_bid", bids.update);//listo
router.put("/billingCycle/:pk_id_billing_cycle", billingCycle.update);//listo
router.put("/billingData/:pk_id_billing_data", billingData.update);//listo


module.exports = router;