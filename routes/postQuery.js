const express = require("express");
const router = express.Router();
const payMethod = require("./../controllers/payMethod");
const piecestoShow = require("./../controllers/piecestoShow");
const status = require("./../controllers/status");
const users = require("./../controllers/users");
const professionalRole = require("./../controllers/professionalRole");
const historyUser = require("./../controllers/historyUser");
const gallery = require("./../controllers/gallery");
const galleryType = require("./../controllers/galleryType");
const statsGallery = require("./../controllers/statsGallery");
const exposition = require("./../controllers/exposition");
const curatorialText = require("./../controllers/curatorialText");
const fontFamily = require("./../controllers/fontFamily");
const artWork = require("./../controllers/artWork");
const measureUnit = require("./../controllers/measureUnit");
const categoryArtwork = require("./../controllers/categoryArtwork");
const aditionalInfo = require("./../controllers/aditionalInfo");
const pictureFrame = require("./../controllers/pictureFrame");
const pedestal = require("./../controllers/pedestal");
const currency = require("./../controllers/currency");
const statsArtwork = require("./../controllers/statsArtwork");
const shoppingCart = require("./../controllers/shoppingCart");
const deliveryAddress = require("./../controllers/deliveryAddress");
const cartItem = require("./../controllers/cartItem");
const auction = require("./../controllers/auction");
const bids = require("./../controllers/bids");
const billingCycle = require("./../controllers/billingCycle");
const billingData = require("./../controllers/billingData");

router.post("/payMethod", payMethod.insert);//listo
router.post("/piecestoShow", piecestoShow.insert);//listo
router.post("/status", status.insert);//listo
router.post("/users", users.insert);//listo
router.post("/professionalRole", professionalRole.insert);//listo
router.post("/historyUser", historyUser.insert);//listo
router.post("/gallery", gallery.insert);//listo
router.post("/galleryType", galleryType.insert);//listo
router.post("/statsgallery", statsGallery.insert);//listo
router.post("/exposition", exposition.insert);//listo
router.post("/curatorialText", curatorialText.insert);//listo
router.post("/fontFamily", fontFamily.insert);//listo
router.post("/artWork", artWork.insert);//listo
router.post("/measureUnit", measureUnit.insert);//listo
router.post("/categoryArtwork", categoryArtwork.insert);//listo
router.post("/aditionalInfo", aditionalInfo.insert);//listo
router.post("/pictureFrame", pictureFrame.insert);//listo
router.post("/pedestal", pedestal.insert);//listo
router.post("/currency", currency.insert);//listo
router.post("/statsArtwork", statsArtwork.insert);//listo
router.post("/shoppingCart", shoppingCart.insert);//listo
router.post("/deliveryAddress", deliveryAddress.insert);//listo
router.post("/cartItem", cartItem.insert);//listo
router.post("/auction/", auction.insert);//listo
router.post("/bids", bids.insert);//listo
router.post("/billingCycle", billingCycle.insert);//listo
router.post("/billingData", billingData.insert);//listo

module.exports = router;