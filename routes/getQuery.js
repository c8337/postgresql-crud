const express = require("express");
const router = express.Router();
const payMethod = require("./../controllers/payMethod");
const piecestoShow = require("./../controllers/piecestoShow");
const status = require("./../controllers/status");
const users = require("./../controllers/users");
const professionalRole = require("./../controllers/professionalRole");
const historyUser = require("./../controllers/historyUser");
const gallery = require("./../controllers/gallery");
const galleryType = require("./../controllers/galleryType");
const statsGallery = require("./../controllers/statsGallery");
const exposition = require("./../controllers/exposition");
const curatorialText = require("./../controllers/curatorialText");
const fontFamily = require("./../controllers/fontFamily");
const artWork = require("./../controllers/artWork");
const measureUnit = require("./../controllers/measureUnit");
const categoryArtwork = require("./../controllers/categoryArtwork");
const aditionalInfo = require("./../controllers/aditionalInfo");
const pictureFrame = require("./../controllers/pictureFrame");
const pedestal = require("./../controllers/pedestal");
const currency = require("./../controllers/currency");
const statsArtwork = require("./../controllers/statsArtwork");
const shoppingCart = require("./../controllers/shoppingCart");
const deliveryAddress = require("./../controllers/deliveryAddress");
const cartItem = require("./../controllers/cartItem");
const auction = require("./../controllers/auction");
const bids = require("./../controllers/bids");
const billingCycle = require("./../controllers/billingCycle");
const billingData = require("./../controllers/billingData");

router.get("/PayMethod", payMethod.getAll);//listo
router.get("/payMethod/:pk_id_payment_method", payMethod.getSpecific);//listo
router.get("/users", users.getAll);//listo
router.get("/users/:pk_id_users", users.getSpecific);//listo
router.get("/status", status.getAll);//listo
router.get("/status/:pk_id_status", status.getSpecific);//listo
router.get("/piecestoShow", piecestoShow.getAll);//listo
router.get("/piecestoShow/:pk_id_pieces_to_show", piecestoShow.getSpecific);//listo
router.get("/ProfessionalRole", professionalRole.getAll);//listo
router.get("/professionalRole/:pk_id_professional_role", professionalRole.getSpecific);//listo
router.get("/historyUser", historyUser.getAll);//listo
router.get("/historyUser/:pk_id_history_user", historyUser.getSpecific);//listo
router.get("/gallery", gallery.getAll);//listo
router.get("/gallery/:pk_id_gallery", gallery.getSpecific);//listo
router.get("/galleryType", galleryType.getAll);//listo
router.get("/galleryType/:pk_id_gallery_type", galleryType.getSpecific);//listo
router.get("/statsgallery", statsGallery.getAll);//listo
router.get("/statsgallery/:pk_id_stats_gallery", statsGallery.getSpecific);//listo
router.get("/exposition", exposition.getAll);//listo
router.get("/exposition/:pk_id_exposition", exposition.getSpecific);//listo
router.get("/curatorialText", curatorialText.getAll);//listo
router.get("/curatorialText/:pk_id_curatorial_text", curatorialText.getSpecific);//listo
router.get("/fontFamily", fontFamily.getAll);//listo
router.get("/fontFamily/:pk_id_font_family", fontFamily.getSpecific);//listo
router.get("/artWork", artWork.getAll);//listo
router.get("/artWork/:pk_id_art_work", artWork.getSpecific);//listo
router.get("/measureUnit", measureUnit.getAll);//listo
router.get("/measureUnit/:pk_id_measure_unit", measureUnit.getSpecific);//listo
router.get("/categoryArtwork", categoryArtwork.getAll);//listo
router.get("/categoryArtwork/:pk_id_category_art_work", categoryArtwork.getSpecific);//listo
router.get("/aditionalInfo", aditionalInfo.getAll);//listo
router.get("/aditionalInfo/:pk_id_aditional_info", aditionalInfo.getSpecific);//listo
router.get("/pictureFrame", pictureFrame.getAll);//listo
router.get("/pictureFrame/:pk_id_picture_frame", pictureFrame.getSpecific);//listo
router.get("/pedestal", pedestal.getAll);//listo
router.get("/pedestal/:pk_id_pedestal", pedestal.getSpecific);//listo
router.get("/currency", currency.getAll);//listo
router.get("/currency/:pk_id_currency", currency.getSpecific);//listo
router.get("/statsArtwork", statsArtwork.getAll);//listo
router.get("/statsArtwork/:pk_id_stats_artwork", statsArtwork.getSpecific);//listo
router.get("/shoppingCart/", shoppingCart.getAll);//listo
router.get("/shoppingCart/:pk_id_shopping_cart", shoppingCart.getSpecific);//listo
router.get("/deliveryAddress", deliveryAddress.getAll);//listo
router.get("/deliveryAddress/:pk_id_delivery", deliveryAddress.getSpecific);//listo
router.get("/cartItem", cartItem.getAll);//listo
router.get("/cartItem/:pk_id_cart_item", cartItem.getSpecific);//listo
router.get("/auction", auction.getAll);//listo
router.get("/auction/:pk_id_auction", auction.getSpecific);//listo
router.get("/bids", bids.getAll);//listo
router.get("/bids/:pk_id_bid", bids.getSpecific);//listo
router.get("/billingCycle", billingCycle.getAll);//listo
router.get("/billingCycle/:pk_id_billing_cycle", billingCycle.getSpecific);//listo
router.get("/billingData", billingData.getAll);//listo
router.get("/billingData/:pk_id_billing_data", billingData.getSpecific);//listo
module.exports = router;
