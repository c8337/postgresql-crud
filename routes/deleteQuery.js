const express = require("express");
const router = express.Router();
const payMethod = require("./../controllers/payMethod");
const piecestoShow = require("./../controllers/piecestoShow");
const status = require("./../controllers/status");
const users = require("./../controllers/users");
const professionalRole = require("./../controllers/professionalRole");
const historyUser = require("./../controllers/historyUser");
const gallery = require("./../controllers/gallery");
const galleryType = require("./../controllers/galleryType");
const statsGallery = require("./../controllers/statsGallery");
const exposition = require("./../controllers/exposition");
const curatorialText = require("./../controllers/curatorialText");
const fontFamily = require("./../controllers/fontFamily");
const artWork = require("./../controllers/artWork");
const measureUnit = require("./../controllers/measureUnit");
const categoryArtwork = require("./../controllers/categoryArtwork");
const aditionalInfo = require("./../controllers/aditionalInfo");
const pictureFrame = require("./../controllers/pictureFrame");
const pedestal = require("./../controllers/pedestal");
const currency = require("./../controllers/currency");
const statsArtwork = require("./../controllers/statsArtwork");
const shoppingCart = require("./../controllers/shoppingCart");
const deliveryAddress = require("./../controllers/deliveryAddress");
const cartItem = require("./../controllers/cartItem");
const auction = require("./../controllers/auction");
const bids = require("./../controllers/bids");
const billingCycle = require("./../controllers/billingCycle");
const billingData = require("./../controllers/billingData");

router.delete("/payMethod/:pk_id_payment_method", payMethod.delete);//listo
router.delete("/users/:pk_id_users", users.delete);//listo
router.delete("/status/:pk_id_status", status.delete);//listo
router.delete("/piecestoShow/:pk_id_pieces_to_show", piecestoShow.delete);//listo
router.delete("/professionalRole/:pk_id_professional_role", professionalRole.delete);//listo
router.delete("/historyUser/:pk_id_history_user", historyUser.delete);//listo
router.delete("/gallery/:pk_id_gallery", gallery.delete);//listo
router.delete("/galleryType/:pk_id_gallery_type", galleryType.delete);//listo
router.delete("/statsGallery/:pk_id_stats_gallery", statsGallery.delete);//listo
router.delete("/exposition/:pk_id_exposition", exposition.delete);//listo
router.delete("/curatorialText/:pk_id_curatorial_text", curatorialText.delete);//listo
router.delete("/artWork/:pk_id_art_work", artWork.delete);//listo
router.delete("/measureUnit/:pk_id_measure_unit", measureUnit.delete);//listo
router.delete("/categoryArtwork/:pk_id_category_art_work", categoryArtwork.delete);//listo
router.delete("/aditionalInfo/:pk_id_aditional_info", aditionalInfo.delete);//listo
router.delete("/fontFamily/:pk_id_font_family", fontFamily.delete);//listo
router.delete("/pictureFrame/:pk_id_picture_frame", pictureFrame.delete);//listo
router.delete("/pedestal/:pk_id_pedestal", pedestal.delete);//listo
router.delete("/currency/:pk_id_currency", currency.delete);//listo
router.delete("/statsArtwork/:pk_id_stats_artwork", statsArtwork.delete);//listo
router.delete("/shoppingCart/:pk_id_shopping_cart", shoppingCart.delete);//listo
router.delete("/deliveryAddress/:pk_id_delivery", deliveryAddress.delete);//listo
router.delete("/cartItem/:pk_id_cart_item", cartItem.delete);//listo
router.delete("/auction/:pk_id_auction", auction.delete);//listo
router.delete("/bids/:pk_id_bid", bids.delete);//listo
router.delete("/billingCycle/:pk_id_billing_cycle", billingCycle.delete);//listo
router.delete("/billingData/:pk_id_billing_data", billingData.delete);//listo

module.exports = router;